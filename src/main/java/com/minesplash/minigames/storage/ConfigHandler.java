package com.minesplash.minigames.storage;

import com.minesplash.minigames.Minigames;
import org.bukkit.configuration.file.FileConfiguration;

public class ConfigHandler {

    private FileConfiguration file;
    private Minigames plugin;

    public ConfigHandler(FileConfiguration file,Minigames plugin){
        this.file = file;
        this.plugin = plugin;
    }

    public String getWSURI(){
        return file.getString("WSUri");
    }

    public String getEIP(){
        return file.getString("ExternalIP");
    }
}
