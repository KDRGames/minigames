package com.minesplash.minigames.storage;

import org.bukkit.configuration.file.FileConfiguration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class StorageHandler {

    private DataSource dataSource;

    public StorageHandler(FileConfiguration config){
        this.dataSource = new DataSource(config.getString("database.url"),config.getString("database.username"),config.getString("database.password"));
    }

    public void disconnect(){
        try {
            this.dataSource.getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
