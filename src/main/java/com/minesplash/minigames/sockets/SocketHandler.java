package com.minesplash.minigames.sockets;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.minesplash.minigames.Minigames;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.simple.JSONObject;

import java.net.URI;
import java.util.logging.Logger;

public class SocketHandler {

    private WebSocketClient cc;
    private Minigames plugin;
    private Logger logger;

    public SocketHandler(String location,String externalIP, Minigames plugin){
        this.plugin = plugin;
        this.logger = this.plugin.getLogger();
        try {
            this.cc = new WebSocketClient(new URI(location)) {
                @Override
                public void onOpen(ServerHandshake handshakedata) {
                    logger.info(">>>Connected to "+location+" Successfully!");
                    JsonObject packet = new JsonObject();
                    packet.addProperty("messageType",enums.messageType.NEWSERVER.ordinal());
                    packet.addProperty("ip",externalIP+":"+plugin.getServer().getPort());
                    packet.addProperty("status",enums.statusType.READY.ordinal());
                    packet.addProperty("type",enums.serverType.MINIGAME.ordinal());
                    this.send(packet.toString());
                }

                @Override
                public void onMessage(String message) {
                    logger.info(">>>New message: "+message);
                    JsonObject packet = new JsonParser().parse(message).getAsJsonObject();
                    switch(enums.messageType.values()[packet.get("messageType").getAsInt()]){
                        case STARTGAME:
                            enums.gameMode mode = enums.gameMode.values()[packet.get("gameMode").getAsInt()];
                            try {
                                plugin.setGamemode(mode);
                            } catch (IllegalAccessException | InstantiationException e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                    plugin.updateStatus(enums.statusType.STARTING);
                }

                @Override
                public void onClose(int code, String reason, boolean remote) {
                    logger.warning(">>>Connection closed with code "+code+" with reason "+reason);
                    //TODO:Reconnect
                }

                @Override
                public void onError(Exception ex) {
                    ex.printStackTrace();
                }
            };
        }catch (java.net.URISyntaxException e){
            this.logger.warning(e.toString());
        }

        this.cc.connect();
    }

    public void send(String string){
        this.cc.send(string);
    }

    public void disconnect(){
        this.cc.close();
    }
}
