package com.minesplash.minigames.sockets;

import com.minesplash.minigames.games.AbstractMinigame;
import com.minesplash.minigames.games.bedwars.Bedwars;
import com.minesplash.minigames.games.idle.Idle;

public class enums {
    public enum serverType{
        MINIGAME,
        LOBBY,
        BUNGEE
    }

    public enum statusType{
        READY,
        STARTING,
        RUNNING
    }

    public enum messageType{
        NEWSERVER,
        STATUSUPDATE,
        STARTGAME,
        GAMESTARTED,
        READYGAMES,
        REMOVESERVER;

        messageType(){}
    }

    public enum gameMode{
        IDLE("ID","A server waiting to happen!",9001,Idle.class),
        BEDWARS("BW","KILL THE BED ;)",16,Bedwars.class);

        private final String id;
        private final String motd;
        private final int maxPlayers;
        private final Class<?extends AbstractMinigame> c;

        gameMode(String id, String motd, int maxPlayers, Class<?extends AbstractMinigame> c){
            this.id = id;
            this.motd = motd;
            this.maxPlayers = maxPlayers;
            this.c = c;
        }

        public String getId(){return this.id;}
        public String getMotd(){return this.motd;}
        public int getMaxPlayers(){return this.maxPlayers;}
        public Class<?extends AbstractMinigame> getC(){return this.c;}
    }
}
