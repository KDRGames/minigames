package com.minesplash.minigames;

import com.google.gson.JsonObject;
import com.minesplash.minigames.games.Minigame;
import com.minesplash.minigames.sockets.SocketHandler;
import com.minesplash.minigames.sockets.enums;
import com.minesplash.minigames.storage.ConfigHandler;
import com.minesplash.minigames.storage.StorageHandler;
import org.bukkit.plugin.java.JavaPlugin;

public final class Minigames extends JavaPlugin {

    private ConfigHandler configHandler;
    private StorageHandler storageHandler;
    private SocketHandler socketHandler;
    private enums.gameMode gamemode;
    private enums.statusType status;
    private Minigame game;
    private MainListener listener;

    @Override
    public void onEnable() {
        // Plugin startup logic
        this.saveDefaultConfig();

        this.configHandler = new ConfigHandler(this.getConfig(),this);
        this.storageHandler = new StorageHandler(this.getConfig());
        this.socketHandler = new SocketHandler(this.configHandler.getWSURI(),this.configHandler.getEIP(),this);

        try {
            this.setGamemode(enums.gameMode.IDLE);
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }

        this.status = enums.statusType.READY;

        this.listener = new MainListener(this,this.game);

        this.getServer().getPluginManager().registerEvents(this.listener,this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        this.storageHandler.disconnect();
        this.socketHandler.disconnect();
    }

    public void setGamemode(enums.gameMode mode) throws IllegalAccessException, InstantiationException {
        this.gamemode = mode;
        if (this.game != null)
            this.game.onUnload();
        this.game = mode.getC().newInstance();
        if (this.listener != null)
            this.listener.setGame(game);
        this.game.onLoad(this);
    }

    public void updateStatus(enums.statusType status){
        this.status = status;

        JsonObject packet = new JsonObject();
        packet.addProperty("messageType",enums.messageType.STATUSUPDATE.ordinal());
        packet.addProperty("status",this.status.ordinal());

        this.socketHandler.send(packet.toString());
    }
}
