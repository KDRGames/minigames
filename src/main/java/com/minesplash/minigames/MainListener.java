package com.minesplash.minigames;

import com.minesplash.minigames.games.Minigame;
import com.minesplash.minigames.games.idle.Idle;
import com.minesplash.minigames.sockets.enums;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class MainListener implements Listener {

    private Minigame game;
    private Minigames plugin;

    public MainListener(Minigames plugin, Minigame game){
        this.plugin = plugin;
        this.game = game;
    }

    public void setGame(Minigame game){
        this.game = game;
    }

    @EventHandler
    public void onLogin(PlayerJoinEvent e){
        e.setJoinMessage("");
        game.addPlayer(e.getPlayer());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        e.setQuitMessage("");
        game.removePlayer(e.getPlayer());

        //Switch server to Idle if nobody is in the game
        if (game.getPlayers().size() < 1 && !(game instanceof Idle)){
            try {
                plugin.setGamemode(enums.gameMode.IDLE);
            } catch (IllegalAccessException | InstantiationException e1) {
                e1.printStackTrace();
            }
        }
    }
}
