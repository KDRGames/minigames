package com.minesplash.minigames.games;

import com.minesplash.minigames.games.data.Team;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractTeamMinigame extends AbstractMinigame implements TeamMinigame {

    private ArrayList<Team> teams = new ArrayList<>();

    @Override
    public Collection<Team> getTeams() {
        return teams;
    }

    @Override
    public void addTeam(Team team){
        this.teams.add(team);
    }

    @Override
    public void removeTeam(Team team){
        this.teams.remove(team);
    }
}
