package com.minesplash.minigames.games;

import com.minesplash.minigames.games.data.Team;

import java.util.Collection;

public interface TeamMinigame extends Minigame {

    int getTeamSize();

    Collection<Team> getTeams();
    void addTeam(Team team);
    void removeTeam(Team team);
}
