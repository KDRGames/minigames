package com.minesplash.minigames.games.data;

import org.bukkit.Color;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;

public class Team {

    private ArrayList<Player> players;
    private Color color;
    private boolean respawnable;

    public Team(Color color){
        this.players = new ArrayList<>();
        this.color = color;
        this.respawnable = true;
    }

    public Team(Color color,Boolean respawnable){
        this.players = new ArrayList<>();
        this.color = color;
        this.respawnable = respawnable;
    }

    public void addPlayer(Player player){
        this.players.add(player);
    }

    public void removePlayer(Player player){
        this.players.remove(player);
    }

    public Collection<Player> getPlayers(){
        return this.players;
    }

    public Color getColor(){
        return this.color;
    }

    public boolean canRespawn(){
        return this.respawnable;
    }

    public void setRespawnable(Boolean respawnable){
        this.respawnable = respawnable;
    }

    public void broadcast(String message){
        for(Player player: this.players){
            player.sendMessage(message);
        }
    }
}
