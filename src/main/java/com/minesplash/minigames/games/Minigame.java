package com.minesplash.minigames.games;

import com.minesplash.minigames.Minigames;
import org.bukkit.entity.Player;

import java.util.Collection;

public interface Minigame {

    void onLoad(Minigames plugin);
    void onUnload();

    Collection<Player> getPlayers();
    void addPlayer(Player player);
    void removePlayer(Player player);

    void playerJoin(Player player);
    void playerLeave(Player player);
}
