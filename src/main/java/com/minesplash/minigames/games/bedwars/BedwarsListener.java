package com.minesplash.minigames.games.bedwars;

import com.minesplash.minigames.Minigames;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BedwarsListener implements Listener {


    Minigames plugin;

    public BedwarsListener(Minigames plugin){
        this.plugin = plugin;
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event){
        Block block = event.getBlock();

        if (block.getType() == Material.BED_BLOCK){
            //TODO:Actually like do things with teams
            event.getPlayer().sendMessage("You broke a bed!");
        }
    }
}
