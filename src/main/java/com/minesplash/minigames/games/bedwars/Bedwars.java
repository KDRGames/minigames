package com.minesplash.minigames.games.bedwars;

import com.minesplash.minigames.Minigames;
import com.minesplash.minigames.games.AbstractTeamMinigame;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

public class Bedwars extends AbstractTeamMinigame {

    private Minigames plugin;
    private BedwarsListener listener;

    @Override
    public void onLoad(Minigames plugin) {
        this.plugin = plugin;
        this.listener = new BedwarsListener(this.plugin);
        plugin.getServer().getPluginManager().registerEvents(this.listener,this.plugin);
    }

    @Override
    public void onUnload() {
        HandlerList.unregisterAll(listener);
    }

    @Override
    public void playerJoin(Player player) {
        this.broadcast("&9"+player.getDisplayName()+"&1 Wants to break some beds!");
    }

    @Override
    public void playerLeave(Player player) {
        this.broadcast("&c"+player.getDisplayName()+"&4 Changed their mind :/");
    }

    @Override
    public int getTeamSize() {
        return 2;
    }
}
