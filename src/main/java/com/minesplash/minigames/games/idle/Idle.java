package com.minesplash.minigames.games.idle;

import com.minesplash.minigames.Minigames;
import com.minesplash.minigames.games.AbstractMinigame;
import org.bukkit.entity.Player;

public class Idle extends AbstractMinigame {


    @Override
    public void onLoad(Minigames plugin) {

    }

    @Override
    public void onUnload() {
        //Do nothing, this is idle
    }

    @Override
    public void playerJoin(Player player) {
        //Do nothing, this is idle
    }

    @Override
    public void playerLeave(Player player) {
        //Do nothing, this is idle
    }
}
