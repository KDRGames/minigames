package com.minesplash.minigames.games;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractMinigame implements Minigame{

    private ArrayList<Player> players = new ArrayList<>();

    public Collection<Player> getPlayers(){
        return this.players;
    }

    @Override
    public void addPlayer(Player player) {
        players.add(player);
        this.playerJoin(player);
    }

    @Override
    public void removePlayer(Player player){
        players.remove(player);
        this.playerLeave(player);
    }

    public void broadcast(String message){
        message = ChatColor.translateAlternateColorCodes('&',message);
        for(Player player:this.getPlayers()){
            player.sendMessage(message);
        }
    }
}
