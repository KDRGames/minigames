package com.minesplash.minigamesbungee;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.net.InetSocketAddress;

public class ServerUtils {
    public static ServerInfo addServer(String name, String address, String motd, boolean restricted) {
        String[] split = address.split(":");
        String ip = split[0];
        int port = Integer.parseInt(split[1]);
        InetSocketAddress iNet = new InetSocketAddress(ip,port);

        ServerInfo info = ProxyServer.getInstance().constructServerInfo(name, iNet, motd, restricted);

        ProxyServer.getInstance().getServers().put(name, info);

        return info;
    }
    public static void removeServer(String name) {
        for (ProxiedPlayer p : ProxyServer.getInstance().getServerInfo(name).getPlayers()) {
            p.disconnect(new TextComponent("This server was forcefully closed.\nPlease report this error in the bug report section of the forums."));
        }
        ProxyServer.getInstance().getServers().remove(name);
    }
}
