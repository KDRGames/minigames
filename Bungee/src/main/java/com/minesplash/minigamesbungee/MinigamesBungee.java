package com.minesplash.minigamesbungee;

import com.google.gson.JsonObject;
import com.minesplash.minigamesbungee.sockets.SocketHandler;
import com.minesplash.minigamesbungee.sockets.enums;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;

public final class MinigamesBungee extends Plugin {


    private ConfigHandler configHandler;
    private SocketHandler socketHandler;

    private Map<Integer,Server> servers;

    @Override
    public void onEnable() {
        // Plugin startup logic

        this.servers = new HashMap<>();

        this.configHandler = new ConfigHandler(this);
        this.socketHandler = new SocketHandler(this.configHandler.getWSURI(),this);

        this.getProxy().getPluginManager().registerCommand(this, new GameCommand("game",this));
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        socketHandler.disconnect();
    }

    public void addServer(Server server){
        this.servers.put(server.getId(),server);
    }

    public Server getServerById(int id){
        return this.servers.get(id);
    }

    public void removeServer(Server server){
        this.servers.remove(server.getId());
        server.remove();
    }

    public void sendPlayerToGame(ProxiedPlayer player, enums.gameMode mode){
        for (Server server:servers.values()){
            if (server.getGamemode() == mode && server.getStatus() != enums.statusType.RUNNING){
                player.connect(server.getInfo());
                return;
            }
        }
        for (Server server:servers.values()){
            if (server.getGamemode() == enums.gameMode.IDLE){
                this.setServerGame(server,mode);
                player.connect(server.getInfo());
                return;
            }
        }
        //TODO:Improve code for finding available servers (Parties should be taken into account)
    }

    public void setServerGame(Server server, enums.gameMode mode){
        JsonObject packet = new JsonObject();
        packet.addProperty("messageType",enums.messageType.STARTGAME.ordinal());
        packet.addProperty("id",server.getId());
        packet.addProperty("game",mode.ordinal());
        socketHandler.send(packet.toString());
    }
}
