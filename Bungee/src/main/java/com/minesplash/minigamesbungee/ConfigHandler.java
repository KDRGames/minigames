package com.minesplash.minigamesbungee;

import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class ConfigHandler {

    private Configuration file;
    private MinigamesBungee plugin;

    public ConfigHandler(MinigamesBungee plugin){
        this.plugin = plugin;
        try {
            this.file = loadConfiguration();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Configuration loadConfiguration() throws IOException {
        if (!plugin.getDataFolder().exists())
            plugin.getDataFolder().mkdir();

        File file = new File(plugin.getDataFolder(), "config.yml");
        if (!file.exists()) {
            try (InputStream in = plugin.getResourceAsStream("config.yml")) {
                Files.copy(in, file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
    }

    public String getWSURI(){
        return file.getString("WSUri");
    }

}
