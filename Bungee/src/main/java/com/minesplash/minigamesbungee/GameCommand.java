package com.minesplash.minigamesbungee;

import com.minesplash.minigamesbungee.sockets.enums;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class GameCommand extends Command {

    MinigamesBungee plugin;

    public GameCommand(String name, MinigamesBungee plugin) {
        super(name);
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage(new TextComponent(""));
            return;
        }

        if (args.length == 0) {
            sender.sendMessage(new TextComponent("/game <gamemode>. Availible gamemodes: "));
            //TODO:List gamemodes.
            return;
        }

        String mode = args[0].toUpperCase();
        enums.gameMode gamemode = enums.gameMode.valueOf(mode);
        if (gamemode == null){
            sender.sendMessage(new TextComponent("Unknown gamemode"));
            return;
        }

        plugin.sendPlayerToGame((ProxiedPlayer) sender, gamemode);
    }
}
