package com.minesplash.minigamesbungee.sockets;

public class enums {
    public enum serverType{
        MINIGAME,
        LOBBY,
        BUNGEE
    }

    public enum statusType{
        READY,
        STARTING,
        RUNNING
    }

    public enum messageType{
        NEWSERVER,
        STATUSUPDATE,
        STARTGAME,
        GAMESTARTED,
        READYGAMES,
        REMOVESERVER;

        messageType(){}
    }

    public enum gameMode{
        IDLE("ID","A server waiting to happen!",9001),
        BEDWARS("BW","KILL THE BED ;)",16);

        private final String id;
        private final String motd;
        private final int maxPlayers;

        gameMode(String id,String motd,int maxPlayers){
            this.id = id;
            this.motd = motd;
            this.maxPlayers = maxPlayers;
        }

        public String getId(){return this.id;}
        public String getMotd(){return this.motd;}
        public int getMaxPlayers(){return this.maxPlayers;}
    }
}
