package com.minesplash.minigamesbungee.sockets;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.minesplash.minigamesbungee.MinigamesBungee;
import com.minesplash.minigamesbungee.Server;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.logging.Logger;

public class SocketHandler {

    private WebSocketClient cc;
    private MinigamesBungee plugin;
    private Logger logger;

    public SocketHandler(String location, MinigamesBungee plugin){
        this.plugin = plugin;
        this.logger = this.plugin.getLogger();
        try {
            this.cc = new WebSocketClient(new URI(location)) {
                @Override
                public void onOpen(ServerHandshake handshakedata) {
                    logger.info(">>>Connected to "+location+" Successfully!");
                    JsonObject packet = new JsonObject();
                    packet.addProperty("messageType",enums.messageType.NEWSERVER.ordinal());
                    packet.addProperty("status",enums.statusType.READY.ordinal());
                    packet.addProperty("type",enums.serverType.BUNGEE.ordinal());
                    this.send(packet.toString());
                }

                @Override
                public void onMessage(String message) {
                    //logger.info(">>>New message: "+message);
                    JsonObject packet = new JsonParser().parse(message).getAsJsonObject();
                    switch(enums.messageType.values()[packet.get("messageType").getAsInt()]){
                        case NEWSERVER:
                            JsonObject server = packet.getAsJsonObject("server");
                            int id = server.get("id").getAsInt();
                            String ip = server.get("ip").getAsString();
                            enums.gameMode mode = enums.gameMode.values()[server.get("gameMode").getAsInt()];
                            enums.statusType status = enums.statusType.values()[server.get("status").getAsInt()];
                            plugin.addServer(new Server(id,ip,status,mode));
                            break;
                        case STATUSUPDATE:
                            id = packet.get("id").getAsInt();
                            status = enums.statusType.values()[packet.get("status").getAsInt()];
                            Server server1 = plugin.getServerById(id);
                            if (server1 == null)
                                break;
                            server1.setStatus(status);
                            break;
                        case GAMESTARTED:
                            id = packet.get("id").getAsInt();
                            mode = enums.gameMode.values()[packet.get("gameMode").getAsInt()];
                            status = enums.statusType.values()[packet.get("status").getAsInt()];
                            server1 = plugin.getServerById(id);
                            if (server1 == null)
                                break;
                            server1.setStatus(status);
                            server1.setGamemode(mode);
                            break;
                        case READYGAMES: //I don't think I'll need this...
                            break;
                        case REMOVESERVER:
                            id = packet.get("id").getAsInt();
                            plugin.removeServer(plugin.getServerById(id));
                        default:
                            logger.warning("received invalid message: "+message);
                            break;
                    }
                }

                @Override
                public void onClose(int code, String reason, boolean remote) {
                    logger.warning(">>>Connection closed with code "+code+" with reason "+reason);
                }

                @Override
                public void onError(Exception ex) {
                    ex.printStackTrace();
                }
            };
        }catch (java.net.URISyntaxException e){
            this.logger.warning(e.toString());
        }

        this.cc.connect();
    }

    public void send(String message){
        cc.send(message);
    }

    public void disconnect(){
        this.cc.close();
    }
}
