package com.minesplash.minigamesbungee;

import com.minesplash.minigamesbungee.sockets.enums;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Server {

    private int id;
    private String name;
    private String ip;
    private enums.statusType status;
    private enums.gameMode gamemode;
    private ServerInfo info;

    public Server(int id,String ip, enums.statusType status, enums.gameMode gamemode){
        this.id = id;
        this.ip = ip;
        this.status = status;
        this.gamemode = gamemode;
        this.name = this.getGamemode().getId()+"-"+this.getId();

        this.info = ServerUtils.addServer(this.name,this.ip,this.gamemode.getMotd(),false);
    }

    public int getId(){
        return this.id;
    }

    public enums.statusType getStatus(){
        return this.status;
    }

    public enums.gameMode getGamemode(){
        return this.gamemode;
    }

    public ServerInfo getInfo(){
        return this.info;
    }

    public void setStatus(enums.statusType status){
        this.status = status;
    }

    public void setGamemode(enums.gameMode gamemode){
        if (this.gamemode == gamemode)
            return;
        this.gamemode = gamemode;
        ServerInfo oldServer = info;
        this.name = this.getGamemode().getId()+"-"+this.getId();
        ServerUtils.addServer(this.name,this.ip,this.gamemode.getMotd(),false);
        ServerInfo newServer = ProxyServer.getInstance().getServers().get(this.name);
        for(ProxiedPlayer player:oldServer.getPlayers()){
            player.connect(newServer);
        }
        ProxyServer.getInstance().getServers().remove(oldServer.getName());
        ProxyServer.getInstance().getServers().put(newServer.getName(),newServer);
        this.info = newServer;
    }

    public void remove(){
        ServerUtils.removeServer(this.name);
    }
}
